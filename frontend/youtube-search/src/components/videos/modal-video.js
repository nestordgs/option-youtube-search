import { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay } from '@fortawesome/free-solid-svg-icons';

const ModalVideo = ({video, getLinkVideoFun}) => {
  const [show, setShow] = useState(false);
  const handleShow = () => {
    setShow(true);
  }

  const transformDate = (value) => {
    return new Date(value).toDateString('es-CL');
  }

  return (
    <>
      <div className="w-100 h-100 position-absolute card-image-gradient d-flex align-items-center justify-content-center">
        <span className="video-btn-play" onClick={handleShow}>
          <FontAwesomeIcon icon={faPlay} className="text-white fs-2" />
        </span>
      </div>

      <Modal
        show={show}
        onHide={() => setShow(false)}
        dialogClassName="modal-fullscreen-md-down modal-lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            {video.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col">
              <p>
                <span className="fw-bold">Channel: </span> {video.channel_title}
              </p>
              <p>
                <span className="fw-bold">Description: </span>{video.description}
              </p>
              <p>
                <span className="fw-bold">Published At: </span>{transformDate(video.published_at)}
              </p>
              <p>
                <a href={getLinkVideoFun(video.id, video.type)}
                  className="text-secondary text-decoration-none fst-italic"
                  target="_blank"
                  rel="noreferrer"
                  >
                    Watch Video
                  </a>
              </p>
            </div>
            <div className="col">
              <img src={video.thumbnails.url} className="img-fluid" alt="..."/>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );

}

export default ModalVideo;
