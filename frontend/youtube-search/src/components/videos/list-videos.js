import { Component } from 'react';
import ItemVideo from "./item-video";

class ListVideos extends Component {
  render() {
    const videoList = this.props.videos.map((video) => {
      return (<ItemVideo video={video} key={video.id}/>);
    });

    return (
      <div>
        <div className="row row-cols-1 row-cols-sm-2 row-cols-md-2 row-cols-lg-4 g-3 mt-3">
          {videoList}
        </div>
        <p className="mt-3 bd-pagination text-center">
          { this.props.prev_page && <button type="button" className="btn btn-dark" onClick={() => {this.props.searchByTokenPageFun(this.props.prev_page)}} >Previous Page</button> }
          { this.props.next_page && <button type="button" className="btn btn-dark" onClick={() => {this.props.searchByTokenPageFun(this.props.next_page)}} >Next Page</button> }
        </p>
      </div>
    );
  }
}


export default ListVideos;
