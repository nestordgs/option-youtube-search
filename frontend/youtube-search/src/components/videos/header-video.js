import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExternalLinkAlt, faLayerGroup, faTv } from '@fortawesome/free-solid-svg-icons';
import ModalVideo from "./modal-video";

export const HeaderVideo = ({video, getLinkVideoFun}) => {
  return (
    <header className="card-header p-0 position-relative">
      { video.type === 'playlist' &&
        <span className="position-absolute text-white fw-bold fst-italic is-playlist">
          <span className="fs-7">Playlist </span>
          <FontAwesomeIcon icon={faLayerGroup} className="fs-6" />
        </span>
      }
      { video.type === 'channel' &&
        <span className="position-absolute text-white fw-bold fst-italic is-playlist">
          <span className="fs-7">Channel </span>
          <FontAwesomeIcon icon={faTv} className="fs-6" />
        </span>
      }
      <img src={video.thumbnails.url} className="card-img-top" alt="..." width="120" height="90" />
      {
        video.type === 'video' &&
        <ModalVideo video={video} getLinkVideoFun={getLinkVideoFun}/>
      }
      {
        video.type !== 'video' &&
        <div className="w-100 h-100 bg-dark- position-absolute card-image-gradient d-flex align-items-center justify-content-center">
          <a className="video-btn-play" href={getLinkVideoFun(video.id, video.type)} target="_blank" rel="noreferrer">
            <FontAwesomeIcon icon={faExternalLinkAlt} className="text-white fs-2" />
          </a>
        </div>
      }
    </header>
  );
}
