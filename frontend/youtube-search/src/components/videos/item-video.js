import { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons'
import { HeaderVideo } from "./header-video";


class ItemVideo extends Component {
  transformDate(value) {
    return new Date(value).toDateString('es-CL');
  }

  getLinkVideo(id, type) {
    let url;
    if (type === 'playlist') {
      url = `https://www.youtube.com/playlist?list=${id}`;
    }
    else if (type === 'channel') {
      url = this.getLinkChannel(id)
    } else {
      url = `https://www.youtube.com/watch?v=${id}`;
    }
    return url
  }

  getLinkChannel(channelId) {
    return `https://www.youtube.com/channel/${channelId}`;
  }

  render() {
    return (
      <div className="col" key={this.props.video.id}>
        <figure className="card video-card h-100">
          <HeaderVideo video={this.props.video} getLinkVideoFun={this.getLinkVideo.bind(this)}/>
          <div className="card-body border-bottom-0 pb-2 position-relative">
            <h2 className="card-title">{this.props.video.title}</h2>
            <figcaption className="card-text card-caption card-description">
              {this.props.video.description}
            </figcaption>
            <div className="card-channel mt-2 position-absolute">
              <a
                href={this.getLinkChannel(this.props.video.channel_id)}
                className="text-secondary text-decoration-none fst-italic"
                target="_blank"
                rel="noreferrer"
              >
                <span className="fw-bold">Channel:</span> {this.props.video.channel_title}
              </a>
            </div>
          </div>
          <footer className="card-footer d-flex justify-content-between align-items-center py-0 border-top-0 bg-white">
            <span className="card-date">{this.transformDate(this.props.video.published_at)}</span>
            <a href={this.getLinkVideo(this.props.video.id, this.props.video.type)}
              className="btn btn-link btn-sm text-secondary"
              target="_blank"
              rel="noreferrer"
            >
              <FontAwesomeIcon icon={faExternalLinkAlt} />
            </a>
          </footer>
        </figure>
      </div>
    )
  }
};

export default ItemVideo;
