export const InputSearch = ({searchFun}) => {
  let input;

  return (
    <div className="d-flex align-items-center w-100">
      <form onSubmit={(e) => {
          e.preventDefault();
          searchFun(input.value);
        }}
        className="w-75 me-3"
      >
        <input type="search"
          className="form-control"
          placeholder="Search..."
          aria-label="Search"
          ref={node => { input = node }}
        />
      </form>
    </div>
  )
}
