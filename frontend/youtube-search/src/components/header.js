import { Component } from 'react';
import logo from '../logo.svg'
import { InputSearch } from "./input-search";

class Header extends Component {
  render() {
    return (
      <header>
        <nav className="navbar navbar-expand-lg navbar-light bg-dark">
          <div className="container d-flex gap-3 align-items-center">
            <div className="brand w-25">
              <span className="navbar-brand text-white">
                <img src={logo}  alt="" width="30" height="24" className="d-inline-block align-text-top"/>
                Youtube browser
              </span>
            </div>
            <InputSearch searchFun={this.props.searchFun}/>
          </div>
        </nav>
      </header>
    );
  }
}


export default Header;
