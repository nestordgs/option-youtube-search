import './App.css';
import Header from './components/header'
import ListVideos from "./components/videos/list-videos";
import { Component } from 'react';
import axios from "axios";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
      , prev_page: null
      , next_page: null
      , page_info: {
        totalResults: 0
        , resultsPerPage: 0
      }
      , last_search: null
    };
  }

  getVideos(value) {
    axios.get(`${process.env.REACT_APP_API_ENDPOINT}/search?query_search=${value}`)
      .then((response) => {
        const data = response.data

        if (data.code === 200) {
          this.setState({
            items: data.message.items
            , prev_page: data.message.prev_page
            , next_page: data.message.next_page
            , page_info: data.message.page_info
            , last_search: value
          })
        }
      })
      .catch((error) => {
        console.error(`Something went wrong consulting de API ${process.env.REACT_APP_API_ENDPOINT} => ${error}`);
      })
  }

  getVideosPage(next_page_token) {
    axios.get(`${process.env.REACT_APP_API_ENDPOINT}/search?query_search=${this.state.last_search}&page_token=${next_page_token}`)
      .then((response) => {
        const data = response.data

        if (data.code === 200) {
          this.setState({
            items: data.message.items
            , prev_page: data.message.prev_page
            , next_page: data.message.next_page
            , page_info: data.message.page_info
          })
        }
      })
      .catch((error) => {
        console.error(`Something went wrong consulting de API ${process.env.REACT_APP_API_ENDPOINT} => ${error}`);
      })
  }

  render() {
    const videos = this.state.items;
    return (
      <div className="">
        <Header searchFun={this.getVideos.bind(this)} />
        <div className="container">
          {
            videos.length > 0
              ? <ListVideos videos={videos} prev_page={this.state.prev_page} next_page={this.state.next_page} searchByTokenPageFun={this.getVideosPage.bind(this)}/>
              : <p className="text-center m-3 fs-3">Nothing to show</p>
          }
        </div>
      </div>
    );
  }
}

export default App;
